package day7;

import day5.Login;
import day5.Logout;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Firefox {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver","drivers/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://the-internet.herokuapp.com/login");
        driver.manage().window().maximize();
        day5.Login obj1=new Login(driver);
        obj1.username("tomsmith");
        obj1.password("SuperSecretPassword!");
        obj1.press();
        day5.Logout obj2=new Logout(driver);
        obj2.loginApprove();
        obj2.heading();
        obj2.welcomeMessage();
        obj2.logout();

    }

}
